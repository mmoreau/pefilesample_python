# PeFileSample_python

An example of how to use pefile 

## Dependencies

### Windows
```
    python -m pip install pefile --user
```

## Links

* https://en.wikipedia.org/wiki/Portable_Executable
* https://upload.wikimedia.org/wikipedia/commons/1/1b/Portable_Executable_32_bit_Structure_in_SVG_fixed.svg
* https://securitytrainings.net/pefile/
* https://docs.microsoft.com/en-us/windows/win32/debug/pe-format
* https://github.com/erocarrera/pefile/
* https://github.com/erocarrera/pefile/blob/wiki/UsageExamples.md#introduction