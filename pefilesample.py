import os
import pefile
from datetime import datetime

path = r"C:\Windows\System32\conhost.exe"
pe = pefile.PE(path)

def info():

	print("Magic : ", hex(pe.OPTIONAL_HEADER.Magic))
	print("ImageBase : ", hex(pe.OPTIONAL_HEADER.ImageBase))
	print("AddressOfEntryPoint : ", hex(pe.OPTIONAL_HEADER.AddressOfEntryPoint))
	print("NumberOfRvaAndSizes : ", hex(pe.OPTIONAL_HEADER.NumberOfRvaAndSizes))
	print("NumberOfSections : ", hex(pe.FILE_HEADER.NumberOfSections))
	print("Machine Architecture :", hex(pe.FILE_HEADER.Machine))
	print("BaseOfCode :", hex(pe.OPTIONAL_HEADER.BaseOfCode))
	print("DllCharacteristics :", hex(pe.OPTIONAL_HEADER.DllCharacteristics))
	print("TimeDateStamp :", pe.FILE_HEADER.TimeDateStamp, "=>", datetime.fromtimestamp(pe.FILE_HEADER.TimeDateStamp).strftime('%d-%m-%Y %H:%M:%S'))


def section():
	[print(section.name, hex(section.VirtualAddress), hex(section.Misc_VirtualSize), section.SizeOfRawData) for section in pe.sections]


def directory_entry_import():
	[print(entry.dll.decode(), "#", hex(imp.address), imp.name.decode()) for entry in pe.DIRECTORY_ENTRY_IMPORT for imp in entry.imports]


def optional_header_data_directory():
	[print(d_dir, "\n") for data in pe.OPTIONAL_HEADER.DATA_DIRECTORY]


#info()
#section()
directory_entry_import()
#optional_header_data_directory()

os.system("pause")